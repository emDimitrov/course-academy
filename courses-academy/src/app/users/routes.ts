import { UsersComponent } from './users.component';
import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        component: UsersComponent,
        children: [
            {
                path: 'list',
                component: UsersListComponent
            }
        ]
    }
];