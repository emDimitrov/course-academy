export interface User {
    id: number;
    username: string;
    password: string;
    email: string;
    isAdmin?: boolean;
    isBlocked: boolean;
}
