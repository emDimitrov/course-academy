import { Component, OnInit } from '@angular/core';
import AuthService from '../../auth/auth.service';
import { User } from '../models/user/user.model';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  users: User[] = [];
  currentUser: User;

  constructor(private usersService: UsersService,
              private authService: AuthService) {

    this.currentUser = this.authService.getLoggedUser();
  }

  ngOnInit() {
    this.usersService.getAll().subscribe((response) => {
      console.log(response);
      this.users = response;
    });
  }

  blockUser(id: number): void {
    const index = this.users.findIndex(u => u.id === id);
    if (index !== -1) {
      this.users.splice(index, 1);
      let user = this.usersService.getById(id).subscribe();
      user.
      this.usersService.update(id).subscribe(() => {
        console.log('USER DELETED');
      });
    }

    
  }
}
