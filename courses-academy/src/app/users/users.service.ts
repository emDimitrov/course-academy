import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { User } from './models/user/user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public constructor(private http: HttpClient) { }

    public getById(id: number): Observable<User> {
        return this.http.get<User>("/user/" + id);
    }

    public getAll(): Observable<User[]> {
        return this.http.get<User[]>("/user/");
    }

    public create(user: User): Observable<User> {
        return this.http.post<User>("/user/", user);
    }

    public update(id: number, user: User): Observable<User> {
        return this.http.put<User>("/user/" + id, user);
    }

    public delete(id: number): Observable<User> {
        return this.http.delete<User>("/user/" + id);
    }

}
